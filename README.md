# Less1.1

## Getting started

Create .env file from sample and set postgresql credentials

```
cp .env.sample .env
```

Run docker compose for build image. Migrations will be run automatically before nginx start.

```
docker compose up -d
```
